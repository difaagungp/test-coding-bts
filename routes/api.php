<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChecklistController;
use App\Http\Controllers\ChecklistItemController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('guest:sanctum')->group(function () {
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('register', [AuthController::class, 'register'])->name('register');
});

Route::middleware('auth:sanctum')->group(function () {
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');
    
    //Checklist
    Route::prefix('checklist')->name('checklist.')->group(function () {
        Route::get('/', [ChecklistController::class, 'index'])->name('index');
        Route::post('/', [ChecklistController::class, 'store'])->name('store');
        Route::delete('/{checklistId}', [ChecklistController::class, 'destroy'])->name('store');

        // Checklist Item
        Route::prefix('{checklistId}/item')->name('item.')->group(function () {
            Route::get('/', [ChecklistItemController::class, 'index'])->name('index');
            Route::post('/', [ChecklistItemController::class, 'store'])->name('store');
            Route::get('/{checklistItemId}', [ChecklistItemController::class, 'edit'])->name('edit');
            Route::put('/{checklistItemId}', [ChecklistItemController::class, 'updateStatus'])->name('updateStatus');
            Route::delete('/{checklistItemId}', [ChecklistItemController::class, 'destroy'])->name('destroy');
            Route::put('/rename/{checklistItemId}', [ChecklistItemController::class, 'updateName'])->name('updateName');
        });
    });
});
