<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /** method for user login and create token */
    public function login(Request $request)
    {
        // Validasi input user
        $request->validate([
            'username'    => ['required', 'string', 'max:255'],
            'password'    => ['required', 'string', 'min:8'],
        ]);

        try {
            $user = User::where('username', $request->username)->first();

            // membandingkan password yang ada di database dan yang user inputkan
            if (!$user || !Hash::check($request->password, $user->password)) {
                return response()->json(['message' => 'Credential not match'], 401);
            }

            // Membuat token untuk user 
            $token = $user->createToken('auth_token')->plainTextToken;

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully Login',
                'token' => $token
            ]);
        } catch (\Throwable $th) {
            info($th);

            return response()->json([
                'status' => 'failed',
                'message' => 'Failed to login',
            ], 500);
        }
    }

    /** method for user register */
    public function register(Request $request)
    {
        // Validasi input user
        $data_validate = $request->validate([
            'username'    => ['required', 'string', 'max:255', 'unique:users,username'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'password'    => ['required', 'string', 'min:8'],
        ]);

        try {
            // Menambahkan data yang sudah diinputkan user ke database
            User::create(Arr::except($data_validate, ['password']) + [
                'password' => Hash::make($data_validate['password'])
            ]);

            return response()->json([
                'status' => 'success',
                'message' => 'Register Successfully, you can login now',
            ]);
        } catch (\Throwable $th) {
            info($th);
            return response()->json([
                'status' => 'failed',
                'message' => 'Failed to register',
            ], 500);
        }
    }

    /** method for user logout */
    public function logout(Request $request)
    {
        try {
            $request->user()->currentAccessToken()->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Logout Successfully'
            ]);
        } catch (\Throwable $th) {
            info($th);

            return response()->json([
                'status' => 'failed',
                'message' => 'Failed to logout',
            ], 500);
        }
    }
}
