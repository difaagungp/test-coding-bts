<?php

namespace App\Http\Controllers;

use App\Models\Checklist;
use Illuminate\Http\Request;

class ChecklistController extends Controller
{
    /** Listing All Checklist */
    public function index()
    {
        try {
            // Ambil data dari database
            $checklists = Checklist::all();

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully retrieve checklist',
                'data' => $checklists
            ]);
        } catch (\Throwable $th) {
            info($th);

            return response()->json([
                'status' => 'failed',
                'message' => 'Failed retrieve checklist',
            ], 500);
        }
    }

    /** Add New Checklist */
    public function store(Request $request)
    {
        // Validasi Input User
        $data_validate = $request->validate([
            'name' => ['required', 'string', 'max:255']
        ]);
        try {
            // Menambahkan data ke database
            Checklist::create($data_validate);

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully create checklist',
            ]);
        } catch (\Throwable $th) {
            info($th);

            return response()->json([
                'status' => 'failed',
                'message' => 'Failed create checklist',
            ], 500);
        }
    }

    /** Delete Checklist */
    public function destroy($checklistId)
    {
        try {
            // Mencari id checklist
            $checklist = Checklist::find($checklistId);

            // Menghapus data checklist dengan id diatas
            $checklist->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully delete checklist',
            ]);
        } catch (\Throwable $th) {
            info($th);

            return response()->json([
                'status' => 'failed',
                'message' => 'Failed delete checklist',
            ], 500);
        }
    }
}
