<?php

namespace App\Http\Controllers;

use App\Models\Checklist;
use App\Models\ChecklistItem;
use Illuminate\Http\Request;

class ChecklistItemController extends Controller
{
    /** Get All Item Checklist */
    public function index($checklistId)
    {
        try {
            // Mencari data checklist
            $checklist = Checklist::find($checklistId);

            // Mendapatkan data checklist item berdasarkan relasi yang sudah dibuat
            $checklistItems = $checklist->checklistItems()->get();

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully retrieve all checklist item',
                'data' => $checklistItems
            ]);
        } catch (\Throwable $th) {
            info($th);

            return response()->json([
                'status' => 'failed',
                'message' => 'Failed retrieve checklist item',
            ], 500);
        }
    }

    /** Add New Item Checklist */
    public function store(Request $request, $checklistId)
    {
        // Validasi Input User
        $data_validate = $request->validate([
            'itemName' => ['required', 'string', 'max:255']
        ]);
        try {
            //Mencari data checklist
            $checklist = Checklist::find($checklistId);

            // Menambahkan data checklist item berdasarkan relasi yang sudah dibuat
            $checklist->checklistItems()->create([
                'item_name' => $data_validate['itemName']
            ]);

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully create checklist item',
            ]);
        } catch (\Throwable $th) {
            info($th);

            return response()->json([
                'status' => 'failed',
                'message' => 'Failed create checklist',
            ], 500);
        }
    }

    /** Get Item Checklist By Id */
    public function edit($checklistId, $checklistItemId)
    {
        try {
            // Mencari data checklist item
            $checklistItem = ChecklistItem::find($checklistItemId);

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully retrieve checklist item data',
                'data' => $checklistItem
            ]);
        } catch (\Throwable $th) {
            info($th);

            return response()->json([
                'status' => 'failed',
                'message' => 'Failed retrieve checklist item data',
            ], 500);
        }
    }

    /** Update Status Item Checklist */
    public function updateStatus($checklistId, $checklistItemId)
    {
        try {
            // Mencari data checklist item
            $checklistItem = ChecklistItem::find($checklistItemId);

            // Logic update status checklist item
            if ($checklistItem->status) {
                $checklistItem->update([
                    'status' => 0
                ]);
            } else {
                $checklistItem->update([
                    'status' => 1
                ]);
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully update status checklist item',
            ]);
        } catch (\Throwable $th) {
            info($th);

            return response()->json([
                'status' => 'failed',
                'message' => 'Failed update status checklist item',
            ], 500);
        }
    }

    /** Delete Item Checklist */
    public function destroy($checklistId, $checklistItemId)
    {
        try {
            // Mencari data checklist item
            $checklistItem = ChecklistItem::find($checklistItemId);

            // Menghapus data checklist item dari database
            $checklistItem->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully delete checklist item',
            ]);
        } catch (\Throwable $th) {
            info($th);

            return response()->json([
                'status' => 'failed',
                'message' => 'Failed delete checklist item',
            ], 500);
        }
    }

    /** Update Name Item Checklist */
    public function updateName(Request $request, $checklistId, $checklistItemId)
    {
        // Validasi input user
        $data_validate = $request->validate([
            'itemName' => ['required', 'string', 'max:255']
        ]);
        try {
            // Mencari data checklist Item
            $checklistItem = ChecklistItem::find($checklistItemId);

            // Update data nama checklist Item
            $checklistItem->update([
                'item_name' => $data_validate['itemName']
            ]);

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully update name checklist item',
            ]);
        } catch (\Throwable $th) {
            info($th);

            return response()->json([
                'status' => 'failed',
                'message' => 'Failed update name checklist item',
            ], 500);
        }
    }
}
