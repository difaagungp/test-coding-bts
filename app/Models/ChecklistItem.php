<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChecklistItem extends Model
{
    use HasFactory;

    /** The attributes that aren't mass assignable. */
    protected $guarded = [];

    /** Relation one to one with checklist */
    public function checklist()
    {
        return $this->hasOne(Checklist::class, 'id', 'checklist_id');
    }
}
