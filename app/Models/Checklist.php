<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    use HasFactory;

    /** The attributes that aren't mass assignable. */
    protected $guarded = [];

    /** Relation one to many with checklist item */
    public function checklistItems()
    {
        return $this->hasMany(ChecklistItem::class, 'checklist_id', 'id');
    }
}
